/*

	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.


	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

// 1
function addTwoNumbers(num1, num2) {
  console.log('Displayed sum of ' + num1 + ' of ' + num2);
  return num1 + num2;
}
console.log(addTwoNumbers(5, 15));

function substractTwoNumbers(num1, num2) {
  console.log('Displayed sum of ' + num1 + ' of ' + num2);
  return num1 - num2;
}
console.log(substractTwoNumbers(20, 5));

function multipyTwoNumbers(num1, num2) {
  console.log('Displayed sum of ' + num1 + ' of ' + num2);
  return num1 * num2;
}
console.log(multipyTwoNumbers(50, 10));

function quotientTwoNumbers(num1, num2) {
  console.log('Displayed sum of ' + num1 + ' of ' + num2);
  return num1 / num2;
}
console.log(quotientTwoNumbers(50, 10));

let product = multipyTwoNumbers(2, 2);
console.log(product);

let quotient = quotientTwoNumbers(20, 5);
console.log(quotient);

function circleRadius(radius) {
  let area = Math.PI * (radius * radius);
  console.log(
    'The result of getting the area of a circle with: ' + radius + ' radius: '
  );
  console.log(Math.round(area * 100) / 100);
}

circleRadius(15);

let circleArea = circleRadius(20);

function averageScore(avg1, avg2, avg3, avg4) {
  let addScores = avg1 + avg2 + avg3 + avg4;
  let average = addScores / 4;
  console.log(
    'the average of ' + avg1 + ' ' + avg2 + ' ' + avg3 + ' ' + avg4 + ' : '
  );

  console.log(average);
}

averageScore(20, 40, 60, 80);

let averageVar = averageScore(20, 40, 60, 80);

function passbyPercentage(score, totalScore) {
  let percent = 75;
  let solution = (totalScore * score) / percent;
  console.log('is ' + score + '/' + totalScore + ' a passing score?');
  console.log(solution);
}

passbyPercentage(38, 50);
console.log(true);

let isPassed = passbyPercentage(38, 50);
